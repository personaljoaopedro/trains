<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="/css/home.css">
</head>
<body>
	<div class="container">
		<div class="col-sm-12">
		<div class="card card-graph">
			<jsp:include page="components/error.jsp">
        			<jsp:param name="error" value="${error}"/>
   			</jsp:include>
		  	<div class="card-body">
		    	<h5 class="card-title">Importar arquivo de grafo</h5>
		    	<p class="card-text">Para importar o grafo, as cidades devem ser nomeadas usando letras de A a D. As rotas entre duas cidades (A at� B) com uma dist�ncia de 5 � representada como AB5. Rotas devem ser separadas por v�rgula.</p>
		    	<p class="card-text">Cont�udo exemplo: AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7</p>
		    	<form method="POST" enctype="multipart/form-data" action="/trains">
		  			<div class="form-group">
		    			<input type="file" name="graph" class="form-control-file" id="graphFile">
		  			</div>
		  			<button type="submit" class="btn btn-primary">Carregar</button>
				</form>
		  	</div>
		</div>
		</div>	
	</div>
</body>
</html>