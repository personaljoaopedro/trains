package br.com.challenge.joaopedrobc.trains.service.exceptions;

public abstract class DefaultException extends Exception{

	private static final long serialVersionUID = 1L;

	public abstract String getErrorMessage();
}
