package br.com.challenge.joaopedrobc.trains.service.exceptions;

public class InvalidVertexException extends DefaultException {

	private static final long serialVersionUID = 1L;
	private char vertexId;
	private String edge;

	public char getVertexId() {
		return vertexId;
	}

	public String getEdge() {
		return edge;
	}

	public InvalidVertexException(char vertexId, String edge) {
		this.vertexId = vertexId;
		this.edge = edge;
	}

	@Override
	public String getErrorMessage() {
		return "Vértice inválido " + getVertexId() + " na aresta " + getEdge();
	}

}
