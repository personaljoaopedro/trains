package br.com.challenge.joaopedrobc.trains;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainsApplication{


	public static void main(String[] args) throws Exception {
		SpringApplication.run(TrainsApplication.class, args);
	}
}