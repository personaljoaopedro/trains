package br.com.challenge.joaopedrobc.trains.model;

public class Edge {
	
	private String id;
	private Integer weight;
    private Vertex source;
    private Vertex destination;
    
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public Integer getWeight() {
		return weight;
	}
	
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	public Vertex getSource() {
		return source;
	}
	
	public void setSource(Vertex source) {
		this.source = source;
	}
	
	public Vertex getDestination() {
		return destination;
	}
	
	public void setDestination(Vertex destination) {
		this.destination = destination;
	}
	
	public String toString() {
		return source.toString() + destination.toString() + weight;
	}
}
