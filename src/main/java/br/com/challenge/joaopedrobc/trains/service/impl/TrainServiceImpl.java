package br.com.challenge.joaopedrobc.trains.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.challenge.joaopedrobc.trains.model.Graph;
import br.com.challenge.joaopedrobc.trains.service.GraphService;
import br.com.challenge.joaopedrobc.trains.service.TrainService;
import br.com.challenge.joaopedrobc.trains.service.exceptions.EmptyGraphFileException;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidVertexException;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidWeightException;

@Service
public class TrainServiceImpl implements TrainService {
	
	@Autowired
	private GraphService graphService;

	@Override
	public void processGraphFile(MultipartFile file) throws IOException, EmptyGraphFileException, InvalidVertexException, InvalidWeightException {
		storeFile(file);
		parseFile(file);
	}

	private void parseFile(MultipartFile file) throws IOException, InvalidVertexException, InvalidWeightException {
		String content = new BufferedReader(new InputStreamReader(file.getInputStream())).lines().collect(Collectors.joining("\n"));
		String[] routes = content.split(",");
		Graph graph = graphService.get(routes);
	}

	private File storeFile(MultipartFile file) throws IOException, EmptyGraphFileException {
		if(file.isEmpty()) {
			throw new EmptyGraphFileException();
		}
		
		InputStream initialStream = file.getInputStream();
		File targetFile = new File("src/main/resources/graph_" + System.currentTimeMillis() + ".tmp");
		Files.copy(initialStream,targetFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
		IOUtils.closeQuietly(initialStream);
		
		return targetFile;
	}

}
