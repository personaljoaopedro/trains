package br.com.challenge.joaopedrobc.trains.service.exceptions;

public class InvalidWeightException extends DefaultException {

	private static final long serialVersionUID = 1L;
	private Integer weight;
	private String edge;

	public Integer getWeight() {
		return weight;
	}

	public String getEdge() {
		return edge;
	}

	public InvalidWeightException(Integer weight, String edge) {
		this.weight = weight;
		this.edge = edge;
	}

	@Override
	public String getErrorMessage() {
		return "Distância inválida " + getWeight() + " na aresta " + getEdge();
	}

}
