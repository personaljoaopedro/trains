package br.com.challenge.joaopedrobc.trains.service.exceptions;

public class EmptyGraphFileException extends DefaultException {

	private static final long serialVersionUID = 1L;

	@Override
	public String getErrorMessage() {
		return "Arquivo de grafo vazio ou inexistente";
	}

}
