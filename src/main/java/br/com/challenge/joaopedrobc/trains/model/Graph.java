package br.com.challenge.joaopedrobc.trains.model;

import java.util.List;
import java.util.Set;

public class Graph {
	
	private Set<Vertex> vertexes;
	private List<Edge> edges;
	
	public Set<Vertex> getVertexes() {
		return vertexes;
	}
	
	public void setVertexes(Set<Vertex> vertexes) {
		this.vertexes = vertexes;
	}
	
	public List<Edge> getEdges() {
		return edges;
	}
	
	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}
}
