package br.com.challenge.joaopedrobc.trains.service;

import br.com.challenge.joaopedrobc.trains.model.Graph;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidVertexException;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidWeightException;

public interface GraphService {

	Graph get(String[] routes) throws InvalidVertexException, InvalidWeightException;

}
