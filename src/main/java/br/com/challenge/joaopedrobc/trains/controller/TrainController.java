package br.com.challenge.joaopedrobc.trains.controller;

import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.challenge.joaopedrobc.trains.service.TrainService;
import br.com.challenge.joaopedrobc.trains.service.exceptions.DefaultException;

@Controller
@RequestMapping(value="/trains")
public class TrainController {
	
	private static final Logger LOGGER = LogManager.getLogger(TrainController.class);
	
	@Autowired
	private TrainService trainService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public String initialPage(Model model) {
		return "home";
	}
	
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public String uploadGraphFile(Model model, @RequestParam("graph") MultipartFile file) {
		try {
			trainService.processGraphFile(file);
		} catch (DefaultException e) {
			LOGGER.error("There was an error processing the graph file.", e);
			model.addAttribute("error", e.getErrorMessage());
		} catch (IOException e) {
			LOGGER.error("There was an error processing the graph file.", e);
		}
		
		return "home";
	}
}
