package br.com.challenge.joaopedrobc.trains.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import br.com.challenge.joaopedrobc.trains.service.exceptions.EmptyGraphFileException;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidVertexException;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidWeightException;

public interface TrainService {

	void processGraphFile(MultipartFile file) throws IOException, EmptyGraphFileException, InvalidVertexException, InvalidWeightException;

}
