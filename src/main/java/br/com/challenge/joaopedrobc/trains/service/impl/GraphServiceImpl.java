package br.com.challenge.joaopedrobc.trains.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.challenge.joaopedrobc.trains.model.Edge;
import br.com.challenge.joaopedrobc.trains.model.Graph;
import br.com.challenge.joaopedrobc.trains.model.Vertex;
import br.com.challenge.joaopedrobc.trains.service.GraphService;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidVertexException;
import br.com.challenge.joaopedrobc.trains.service.exceptions.InvalidWeightException;

@Service
public class GraphServiceImpl implements GraphService{

	private static final int SOURCE_POSITION = 0;
	private static final int DESTINATION_POSITION = 1;
	private static final int WEIGHT_POSITION = 2;

	@Override
	public Graph get(String[] fileEdges) throws InvalidVertexException, InvalidWeightException {
		List<Edge> edges = new ArrayList<>();
		Set<Vertex> vertexes = new HashSet<>();
		
		for (String fileEdge : fileEdges) {
			if(!StringUtils.isEmpty(fileEdge)) {
				fileEdge = fileEdge.trim();
				Vertex source = getSourceVertex(fileEdge);
				Vertex destination = getDestinationVertex(fileEdge);
				Integer weight = getWeight(fileEdge);
				
				Edge edge = new Edge();
				edge.setSource(source);
				edge.setDestination(destination);
				edge.setWeight(weight);
				edge.setId(fileEdge);
				edges.add(edge);
				
				vertexes.add(source);
				vertexes.add(destination);
			}
		}
		
		Graph graph = new Graph();
		graph.setEdges(edges);
		graph.setVertexes(vertexes);
		
		return graph;
	}

	private Integer getWeight(String edge) throws InvalidWeightException {
		Integer weight = Character.getNumericValue(edge.charAt(WEIGHT_POSITION));
		checkWeight(weight, edge);
		return weight;
	}

	private Vertex getDestinationVertex(String edge) throws InvalidVertexException {
		char id = edge.charAt(DESTINATION_POSITION);
		return getVertex(edge, id);
	}

	private Vertex getSourceVertex(String edge) throws InvalidVertexException {
		char id = edge.charAt(SOURCE_POSITION);
		return getVertex(edge, id);
	}

	private Vertex getVertex(String edge, char id) throws InvalidVertexException {
		checkValidVertex(id, edge);
		return new Vertex(Character.toString(id));
	}

	private void checkValidVertex(char id, String route) throws InvalidVertexException {
		if(!Character.isLetter(id)) {
			throw new InvalidVertexException(id, route);
		}
	}
	
	private void checkWeight(Integer weight, String edge) throws InvalidWeightException {
		if(weight <= 0) {
			throw new InvalidWeightException(weight, edge);
		}
	}

}
